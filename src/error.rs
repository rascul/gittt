use std::fmt::{Display, Formatter, Result as FmtResult};
use std::io::Error as IoError;
use std::num::ParseIntError;

use git2::Error as GitError;
use rss::Error as RssError;

pub type GitttResult<T> = Result<T, GitttError>;

#[derive(Debug)]
pub enum GitttError {
	Git(GitError),
	Io(IoError),
	ParseInt(ParseIntError),
	Rss(RssError),
	Custom(String),
}

impl From<GitError> for GitttError {
	fn from(e: GitError) -> GitttError {
		GitttError::Git(e)
	}
}

impl From<IoError> for GitttError {
	fn from(e: IoError) -> GitttError {
		GitttError::Io(e)
	}
}

impl From<ParseIntError> for GitttError {
	fn from(e: ParseIntError) -> GitttError {
		GitttError::ParseInt(e)
	}
}

impl From<RssError> for GitttError {
	fn from(e: RssError) -> GitttError {
		GitttError::Rss(e)
	}
}

impl<'a> From<&'a str> for GitttError {
	fn from(e: &'a str) -> GitttError {
		GitttError::Custom(e.to_string())
	}
}

impl From<String> for GitttError {
	fn from(e: String) -> GitttError {
		GitttError::Custom(e)
	}
}

impl Display for GitttError {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		match *self {
			GitttError::Git(ref e) => Display::fmt(e, f),
			GitttError::Io(ref e) => Display::fmt(e, f),
			GitttError::ParseInt(ref e) => Display::fmt(e, f),
			GitttError::Rss(ref e) => Display::fmt(e, f),
			GitttError::Custom(ref e) => Display::fmt(e, f),
		}
	}
}
