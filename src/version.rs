use std::cmp::Ordering;
use std::fmt;
use std::str::FromStr;

use error::{GitttError, GitttResult};

#[derive(Debug)]
pub struct Version {
	major: i16,
	minor: i16,
	patch: i16,
	string: String,
	original: String,
}

impl Default for Version {
	fn default() -> Version {
		Version {
			major: 0,
			minor: 0,
			patch: 0,
			string: "default".to_string(),
			original: "default".to_string(),
		}
	}
}

impl fmt::Display for Version {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{}", self.original)
	}
}

impl Eq for Version {}

impl PartialEq for Version {
	fn eq(&self, other: &Version) -> bool {
		self.major == other.major && self.minor == other.minor && self.patch == other.patch
	}
}

impl PartialOrd for Version {
	fn partial_cmp(&self, other: &Version) -> Option<Ordering> {
		Some(self.cmp(other))
	}
}

impl Ord for Version {
	fn cmp(&self, other: &Version) -> Ordering {
		let o = self.major.cmp(&other.major);
		if o == Ordering::Equal {
			let o = self.minor.cmp(&other.minor);
			if o == Ordering::Equal {
				return self.patch.cmp(&other.patch);
			} else {
				return o;
			}
		}

		o
	}
}

impl Version {
	pub fn new(s: &str) -> GitttResult<Version> {
		let mut s = s;
		let orig = s.to_string();
		let mut dots = Vec::new();

		if s.starts_with("v") {
			s = &s[1..];
		}

		for (i, &item) in s.as_bytes().iter().enumerate() {
			if item == b'.' {
				dots.push(i);
			}
		}

		if dots.len() != 2 {
			return Err(GitttError::from(format!(
				"Unable to parse version: {}: not enough dots",
				s
			)));
		}

		if s.len() < dots[1] + 1 {
			return Err(GitttError::from(format!(
				"Unable to parse version: {}: not long enough",
				s
			)));
		}

		let major = i16::from_str(&s[..dots[0]])?;
		let minor = i16::from_str(&s[dots[0] + 1..dots[1]])?;
		let patch = i16::from_str(&s[dots[1] + 1..])?;

		Ok(Version {
			string: format!("{}.{}.{}", &major, &minor, &patch),
			major: major,
			minor: minor,
			patch: patch,
			original: orig,
		})
	}
}
