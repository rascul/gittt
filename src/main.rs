extern crate git2;
extern crate rss;
extern crate tempdir;

mod error;
mod feed;
mod repo;
mod version;

use error::GitttResult;
use feed::Feed;
use repo::Repo;

fn main() -> GitttResult<()> {
	//let repo_url = "https://github.com/tintinplusplus/tintin";
	let repo_url = "file:///home/rascul/projects/gittt/tintin";
	let rss_url = "https://sourceforge.net/projects/tintin/rss?path=/TinTin%2B%2B%20Source%20Code/";

	let repo = Repo::new(repo_url)?;
	let feed = Feed::new(rss_url)?;
	let mut new_tags = Vec::new();

	for tag in &feed.tags {
		if tag > &repo.last_tag {
			println!("NEW TAG: {}", tag);
			new_tags.push(tag);
		}
	}

	new_tags.sort();
	println!("SORTED:");
	for tag in new_tags {
		println!("{}", tag);
	}

	Ok(())
}

//https://          sourceforge.net/projects/tintin/files/TinTin%2B%2B%20Source%20Code/1.94.5/tintin-1.94.5.tar.gz/download
//https://master.dl.sourceforge.net/project/tintin/TinTin%2B%2B%20Source%20Code/1.94.5/tintin-1.94.5.tar.gz
