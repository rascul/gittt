use rss::Channel;

use error::GitttResult;
use version::Version;

pub struct Feed {
	pub url: String,
	pub channel: Channel,
	pub tags: Vec<Version>,
}

impl Feed {
	pub fn new<S: Into<String>>(url: S) -> GitttResult<Feed> {
		let url = url.into();
		let channel = Channel::from_url(&url)?;
		let mut tags = Vec::new();

		for item in channel.items() {
			if let Some(title) = item.title() {
				if let Ok(v) = Version::new(&tag_version(title)) {
					tags.push(v);
				}
			}
		}

		Ok(Feed {
			url: url,
			channel: channel,
			tags: tags,
		})
	}
}

fn tag_version(title: &str) -> String {
	let mut slashes = Vec::new();

	for (i, &item) in title.as_bytes().iter().enumerate() {
		if item == b'/' {
			slashes.push(i);
		}
	}

	title[slashes[1] + 1..slashes[2]].to_string()
}
