use git2::build::RepoBuilder;
use git2::{AutotagOption, FetchOptions, Repository};

use tempdir::TempDir;

use error::{GitttError, GitttResult};
use version::Version;

pub struct Repo {
	pub dir: TempDir,
	pub url: String,
	pub repo: Repository,
	pub last_tag: Version,
}

impl Repo {
	pub fn new<S: Into<String>>(url: S) -> GitttResult<Repo> {
		let url = url.into();
		let dir = TempDir::new("gittt")?;
		let mut last_tag: Version = Default::default();
		let mut repobuilder = RepoBuilder::new();
		let mut fetchoptions = FetchOptions::new();

		fetchoptions.download_tags(AutotagOption::All);
		repobuilder.fetch_options(fetchoptions);

		let repo = repobuilder.clone(&url, &dir.path())?;
		let tags = repo.tag_names(None)?;

		if let Some(tag) = tags.iter().last() {
			if let Some(t) = tag {
				last_tag = Version::new(t)?;
			} else {
				return Err(GitttError::from("No tag found in repo"));
			}
		} else {
			return Err(GitttError::from("No tags found in repo"));
		}

		Ok(Repo {
			dir: dir,
			url: url,
			repo: repo,
			last_tag: last_tag,
		})
	}
}
